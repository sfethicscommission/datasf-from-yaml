#!/usr/bin/env python3
import sys
import os
import logging
import yaml
from dataSF_crud import CRUD

credentials_file = 'credentials.yaml'

# set logging
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

if len(sys.argv) >= 2:
    x = input("This will delete all data within the dataset! Enter 'y' to continue\n")
    if x != "y":
        print('You must type "y" before proceeding. Exiting process')
        sys.exit()

    schema_name = sys.argv[1]
    schema_file_path = os.path.join(
        sys.path[0], 'schemas', 'saved', f'{schema_name}.yaml')

    csv_file = os.path.join(
        sys.path[0], 'schemas', 'create', f'{schema_name}_blank.csv')

    if not os.path.isfile(csv_file):
        print(f'blank data file "{csv_file}" does not exist.')
        sys.exit()

    if os.path.isfile(schema_file_path):
        with CRUD(credentials_file, debug_mode=False) as myclient:
            with open(schema_file_path, 'r') as f:
                schema = yaml.safe_load(f.read())
                if len(sys.argv) > 2 and sys.argv[2] == 'false':
                    myclient.delete(schema_name, schema.get('id'), autobackup=False)
                else:
                    myclient.delete(schema_name, schema.get('id'))
    else: 
        print(
            f'File "{schema_name}.yaml" not found.\n' \
            'Does it exist in directory "./schemas/saved"?'
        )
        sys.exit()
else:
    print('expected an argument: schema_name')
