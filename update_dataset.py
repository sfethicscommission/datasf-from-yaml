#!/usr/bin/env python3
import sys
import os
import logging
from dataSF_crud import CRUD

credentials_file = 'credentials.yaml'

print('need to validate')
sys.exit()
# set logging
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

if len(sys.argv) >= 2:
    schema_name = sys.argv[1]
    root_schema_file = os.path.join(
        sys.path[0], 'schemas', 'root', f'{schema_name}.yaml')
    csv_file = os.path.join(
        sys.path[0], 'upload', f'{schema_name}.csv')

    if os.path.isfile(root_schema_file):
        with CRUD(credentials_file, debug_mode=False) as myclient:
            schema = myclient.fs_read_schema(schema_name)
            myclient.update(schema)
    else: 
        print(
            f'File "{schema_name}.yaml" not found.\n' \
            'Does it exist in directory "./dataset_schemas/"?'
        )
        sys.exit()
else:
    print('expected an argument: schema_name')
