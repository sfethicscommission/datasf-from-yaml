import os
import logging
import sys
import time

from dataSF_crud import CRUD

credentials_file = 'credentials.yaml'

"""
creates a dataset on DataSF using a yaml schema definition


run from command line with one argument:
    schemaname  - "friendly" name which will be used as filename for schema
"""

# set logging
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

if len(sys.argv) >= 2:
    schema_name = sys.argv[1]
    root_schema_file = os.path.join(
        sys.path[0], 'schemas', 'root', f'{schema_name}.yaml')
    csv_file = os.path.join(
        sys.path[0], 'schemas', 'create', f'{schema_name}.csv')
    
    if not os.path.isfile(csv_file):
        print(f'data file {csv_file} does not exist. You may need to run "create" script.')
        sys.exit()

    if os.path.isfile(root_schema_file):
        with CRUD(credentials_file, debug_mode=True) as myclient:
            # read root_schema from fs
            root_schema = myclient.fs_read_schema(schema_name)
            sleepy_time = 120

            # format root_schema for create_step1
            schema_s1 = myclient.format_columns_create_step1(root_schema)
            # create_step1: create the dataaset
            dataset_fourfour = myclient.create(schema_s1)
            logging.info(f"new dataset for {schema_name} is {dataset_fourfour}")

            # build in pause for socrata processing
            # TODO figure out what to call to see if socrata has finished processing or not
            logging.info(f'waiting {sleepy_time} seconds for Socrata to process')
            time.sleep(sleepy_time)

            # # use if the step failed ealier
            # dataset_fourfour = '4imx-7u3b'

            # format root_schema for create_step2
            schema_s2 = myclient.format_columns_create_step2(root_schema, dataset_fourfour)
            # create_step2: update the metadata and columns
            myclient.update(schema_s2, from_scratch=True)

            # build in pause for socrata processing
            logging.info(f'waiting {sleepy_time} seconds for Socrata to process')
            time.sleep(sleepy_time)
            # delete row of "data" from create csv
            myclient.delete(schema_name, dataset_fourfour, autobackup=False)
            
    else:
        print(
            f'File "{schema_name}.yaml" not found.\n' \
            'you may need to run "create" script.'
        )
else:
    print('expected an argument: schema_name')
