# what
a way to create new datasets on DataSF and fill out the metadata using a yaml definition.

- Create new datasets from schema def
- Read metadata for existing datasets
- Update metadata for dataset and columns
- Delete data by uploading empty source file

written for python 3

some of the socrata_create.py methods require pyyaml >= 5.1 to work correctly: 

```shell
$ pip3 install -r requirements.txt
```

TODO: `csv_from_def.py` creates csv with api_name instead of name

# how

*IMPORTANT*:
- rename `example_credentials.yaml` to `credentials.yaml` and fill out with your api key. 
- dataset schemas have to be entirely defined, or columns might disappear

## create a new dataset from csv and update the metadata
- define your dataset and fill out the metadata using an example in `./schemas/input/{schema_name}.yaml`
- define the columns of your dataset using an example csv file in `./schemas/input/{schema_name}.csv`
    - include values to rename the api fieldname of each column (if desired)
- run `$ python3 create_root_schema_def.py {schema_name}` to generate the files the scripts need
- run `$ python3 create_new_dataset.py {schema_name}` to upload the example csv and create the dataset on DataSF, rename the api fieldnames, apply metadata and column descriptions, and delete placeholder row. Refer to dataSF_crud.py object CRUD.__init__() for default metadata values.
- make any desired changes to the dataset using the web GUI on DataSF (eg, set custom row id)

## delete all data from a dataset
This uploads a blank CSV file to the dataset, effectively deleting it.
- schema file should have property "id" with fourfour
- create csv file in `./schemas/create/{schema_name}_blank.csv` containing only headers
    - you can use the `create_root_schema_def.py` to make this file
- run script `$ ./delete_data.py {schema_name}`
    - this will ask for confirmation, hit "y" then "return"
    - a backup copy will first be saved as `./backup/{fourfour}.csv`

# NEEDS TO BE RE-VALIDATED
## update the metadata of an existing dataset
- use the `create_step*.py` scripts to make a new dataset and use that yaml file
- or, define an existing dataset's metadata using the example in `./dataset_schemas/example.yaml`
    - fill in the socrata four-by-four identifier 
    - define all the columns
    - **DO NOT** use rename_api property to rename api field names of datasets as it will destroy data
- or, run the `read_dataset_schema.py` script to generates the yaml file from DataSF 
- edit the metadata in the schema definition yaml file saved as `./dataset_schemas/{schema_name}.yaml`
- run `$ ./update_dataset.py {schema_name}`



# creating datasets for ETH campaign api testing
## filers
- python create_root_schema_def.py filers
- python create_new_dataset.py filers
- log into socrata web ui
- edit dataset > update > upload > replace with csv file(s) from api endpoint
- make filernid column the rowid > update
- update sources in conf/{server}.denorm.tx.yaml and conf/{server}.denorm.tx_private.yaml

## filings
- python create_root_schema_def.py filings
- python create_new_dataset.py filings
- log into socrata web ui
- edit dataset > make filingactivitynid column the rowid > update
- update destination in conf/{server}.sync.filings.yaml
- update sources in conf/{server}.denorm.tx.yaml and conf/{server}.denorm.tx_private.yaml

## transactions
- python create_root_schema_def.py tx
- python create_new_dataset.py tx
- log into socrata web ui (wait until ui shows draft applied)
- (may have to delete existing row)
- edit dataset > make Transaction Nid column the rowid
- update
- update destination in conf/{server}.sync.itemized_tx.yaml
- update destination in conf/{server}.sync.unitemized_tx.yaml
- update target in conf/{server}.denorm.tx.yaml

## transactions - private
- repeat transactions steps but with tx_private

## summary 
- python create_root_schema_def.py summary
- python create_new_dataset.py summary
- log into socrata web ui
- edit dataset > make filingnid column the rowid
- update
- update destination in conf/{server}.sync.filings.yaml (add as second destination)
- update destination in conf/{server}.fillin.summary.yaml (add as second destination)
- run campaign_api_sync for summary




# DataSF MetaData Description
Make sure to fill out the following as required by DataSF's data standards

```html
<strong>A. SUMMARY</strong>
[What is this dataset?  What purpose does it serve? Are there relevant links to your departments website?]

<strong>B. HOW THE DATASET IS CREATED</strong>
[Describe the business process that generates this data]

<strong>C. UPDATE PROCESS</strong>
[How often is this dataset going to be updated on the portal]

<strong>D. HOW TO USE THIS DATASET</strong>
[describe what should an analyst know about this dataset to use appropriately, what to watch out for, what to filter,etc]
```

* include and identify the dataset's primary key
* include `data_as_of` field that indicates when the record (row) was last updated in the source system
* include `data_loaded_at` field that indicates when the data was upload to the portal
