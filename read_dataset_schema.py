#!/usr/bin/env python3
import sys
import os
from dataSF_crud import CRUD

credentials_file = 'credentials.yaml'
# print('need to validate')
# sys.exit()
"""
creates a yaml schema definition of an extant dataset from DataSF

run from command line with two arguments:
    schema_name  - "friendly" name which will be used as filename for schema
    fourfour    - socrata four by four identifier for dataset to scrape
"""

if (len(sys.argv) == 3):
    schema_name = sys.argv[1]
    singlefile = os.path.join(
        sys.path[0], 'schemas', 'saved', f'{schema_name}.yaml')
    fourfour = sys.argv[2]
    if os.path.isfile(singlefile):
        print(
            f'Schema file "{schema_name}.yaml" already exists.\n' \
                'This process will overwrite that file.\n' \
                'You should rename it first.' 
        )
        sys.exit()
    else: 
        with CRUD(credentials_file, debug_mode=False) as myclient:
            myclient.read(schema_name, fourfour)
else:
    print('incorrect number of arguments: expected "schema_name fourfour"')
    sys.exit()
