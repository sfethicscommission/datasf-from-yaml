import copy
import os
from operator import itemgetter
import requests
import json
import yaml
import logging
import time
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from pathlib import Path

"""


'schema' - yaml definition on local fs
'metadata' - representation of metadata from socrata

"""

logger = logging.getLogger(__name__)

DEFAULT_TIMEOUT = 5  # seconds
class TimeoutHTTPAdapter(HTTPAdapter):
    def __init__(self, *args, **kwargs):
        self.timeout = DEFAULT_TIMEOUT
        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]
        super().__init__(*args, **kwargs)

    def send(self, request, **kwargs):
        timeout = kwargs.get("timeout")
        if timeout is None:
            kwargs["timeout"] = self.timeout
        return super().send(request, **kwargs)

class CRUD:
    def __init__(self, credentials_file, debug_mode=False, permission='private'):
        # assumes this file is in project root path
        root_path = os.path.dirname(os.path.abspath(__file__))

        # TODO: pass in credentials object rather than read file
        # file with api keys for Socrata
        creds_file_path = os.path.join(
            root_path, credentials_file)
        with open(creds_file_path) as f:
            creds = yaml.safe_load(f)

        self.root_path = root_path
        self.root_schema_filepath = os.path.join(
            root_path, 'schemas', 'root')
        self.csv_create_filepath = os.path.join(
            root_path, 'schemas', 'create')
        self.fourfour_filepath = os.path.join(
            root_path, 'schemas', 'fourfour')
        self.saved_schema_filepath = os.path.join(
            root_path, 'schemas', 'saved')

        #### define global default values
        self.domain_url = 'https://data.staff.sf.gov'
        self.credentials = (creds['socrata']['keyId'],
                            creds['socrata']['keySecret'])
        self.headers = {
            'Content-Type': 'application/json',
            'X-App-Token': creds['socrata']['token']
            }

        # share all https requests among a single session
        session = requests.Session()
        retry = Retry(
            total=5,
            backoff_factor=2,
            status_forcelist=[429, 500, 502, 503, 504],
            allowed_methods=["GET", "PUT", "POST"]
        )
        adapter = TimeoutHTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        self.session = session

        self.default_meta = {
            'title': 'Default Title',
            'description': 'Default Description',
            'steward_name': 'San Francisco Ethics Commission',
            'steward_email': 'ethics.commission@sfgov.org',
            'publishing_frequency': 'Daily',
            'data_change_frequency': 'As needed',
            'geographic_unit': 'Not applicable',
            'publishing_lag': 'Yes',
            'publishing_lag_in_days': '1',
            'publishing_method': 'Other',
            'automation_candidate': 'Already automated',
            'category': 'City Management and Ethics',
            'department': 'Ethics Commission',
            'tags': [
                'ethics'
            ],
        }

        self.debug = debug_mode
        self.permission = permission

    def __enter__(self):
        """
            This runs as the with block is set up.
            """
        return self

    def __exit__(self, exc_type=None, exc_value=None, traceback=None):
        """
            This runs at the end of a with block. It simply closes the client.
            Exceptions are propagated forward in the program as usual, and
                are not handled here.
            """
        self.close()

    def close(self):
        """
        Close the requests session.
        """
        self.session.close()
        

    # socrata push ops    
    def create_dataset_revision(self, url, body_data):
        rev_response = self.session.post(
            url, 
            json=body_data,
            auth=self.credentials,
            headers=self.headers,
        )

        n = body_data['action']['type']      
        if (n == 'update'):
            logging.info(
                f"New dataset fourfour " \
                f"{rev_response.json()['resource']['fourfour']}"
            )
            logging.info(
                f"Discard link " \
                f"{rev_response.json()['links']['discard']}"
            )
            if (self.debug):
                self.fs_save_out(rev_response.json(),
                                 f'create_step1', debug=True)
        elif (n == 'replace'):
            if (self.debug):
                self.fs_save_out(rev_response.json(),
                                 f'update_step1', debug=True)
        return rev_response

    def create_source(self, rev_response, body_data):
        # Creating a new revision returns the URL you need to create a source
        create_source_uri = rev_response.json()['links']['create_source']
        create_source_url = f'{self.domain_url}{create_source_uri}'

        # body_data['source_type']['type'] options are 
        # Upload (for uploading a new file)
        # or View (for using the existing dataset as the source)
        
        source_response = self.session.post(
            create_source_url, 
            json=body_data, 
            auth=self.credentials,
            headers=self.headers,
        )
        n = body_data['source_type']['type']
        if (n == 'upload'):
            logging.debug('created source')
            if (self.debug):
                self.fs_save_out(source_response.json(),
                                 f'create_step2', debug=True)
        elif (n == 'view'):
            logging.debug('created source')
            if (self.debug):
                self.fs_save_out(source_response.json(),
                                 f'update_step2', debug=True)
        
        return source_response

    def upload_file(self, source_response, file_bin_data):
        # Get the link for uploading bytes from your source response
        upload_uri = source_response.json()['links']['bytes']
        upload_url = f'{self.domain_url}{upload_uri}'
        upload_headers = {'Content-Type': 'text/csv'}

        upload_response = self.session.post(
            upload_url, 
            data=file_bin_data,
            auth=self.credentials,
            headers=upload_headers,
        )
        if (self.debug):
            self.fs_save_out(upload_response.json(),
                          'create_step3', debug=True)

        return upload_response

    def transform_schema(self, create_source_response, schema):
        input_schemas = create_source_response.json()['resource']['schemas']
        latest_input_schema = max(input_schemas, key=itemgetter('id'))
        output_schemas = latest_input_schema['output_schemas']
        latest_output_schema = max(output_schemas, key=itemgetter('id'))

        # Transform schema of specific row
        updated_columns = latest_output_schema['output_columns']

        # map new column descriptions
        updated_columns = self.map_new_schema_to_columns(updated_columns, schema)

        updated_schema = {
            'output_columns': updated_columns
        }
        input_schema_id = latest_input_schema['id']
        transform_schema_link = create_source_response.json()['links']['input_schema_links']['transform'].format(
            input_schema_id=input_schema_id)

        transform_schema_response = self.session.post(
            self.domain_url + transform_schema_link,
            json=updated_schema,
            auth=self.credentials,
            headers=self.headers,
        )
        if (self.debug):
            self.fs_save_out(transform_schema_response.json(),
                          'update_step3', debug=True)
        return transform_schema_response
    
    def apply_revision(self, rev_response, apply_body, stage=''):
        apply_revision_uri = rev_response.json()['links']['apply']
        apply_revision_url = f'{self.domain_url}{apply_revision_uri}'

        apply_revision_response = self.session.put(
            apply_revision_url,
            json = apply_body,
            headers = self.headers,
            auth = self.credentials
        )

        if (stage == 'create'):
            logging.debug('revision applied')
            if (self.debug):
                self.fs_save_out(apply_revision_response.json(),
                                 f'create_step4', debug=True)
        elif (stage == 'update'):
            logging.debug('revision applied')
            if (self.debug):
                self.fs_save_out(apply_revision_response.json(),
                                 f'update_step4', debug=True)
        
        return apply_revision_response

        
    # socrata pull ops
    def get_metadata(self, fourfour):
        url = f'{self.domain_url}/api/views/{fourfour}'
        meta_response = self.session.get(url, auth=self.credentials,
                                        headers=self.headers)
        return meta_response.json()


    # file system ops
    def fs_read_schema(self, schema_name):
        schema_file_path = os.path.join(
            self.root_schema_filepath, f'{schema_name}.yaml')
        with open(schema_file_path, 'r') as f:
            schema = yaml.safe_load(f.read())
        for col in schema['columns']:
            if 'format' not in col:
                col['format'] = None
        schema['schema_name'] = schema_name
        return schema

    def fs_save_out(self, data, filename, debug=False):
        folder = 'debug' if debug else 'schemas'
        file_path = os.path.join(
            self.saved_schema_filepath, f'{filename}.yaml')
        with open(file_path, 'w') as file:
            yaml.safe_dump(data, file,
                           default_flow_style=False, sort_keys=False)

    def backup_dataset(self, fourfour):
        Path("backup").mkdir(parents=True, exist_ok=True)
        filename = f'backup/{fourfour}.csv'

        url = f'https://data.sfgov.org/api/views/{fourfour}/rows.csv?accessType=DOWNLOAD'
        backup = self.session.get(
            url,
            auth=self.credentials,
            stream=True
        )

        with open(filename, 'wb') as fd:
            for chunk in backup.iter_content(chunk_size=128):
                fd.write(chunk)


    # helpers
    def update_schema_columns(self, schema, metadata):
        """
        update the column names in the schema definition using 
        the extant columns from socrata metadata call 
        (to update with "latest" api_name )
        """
        xwalk = [
            ('name', 'display_name'),
            # ('datatype', 'output_soql_type'),
            ('description', 'description'),
            ('api_name', 'field_name'),
        ]
        columns = [{k: col[v] for (k, v) in xwalk}
                   for col in metadata['resource']['output_columns']]
        schema['columns'] = columns
        return schema

    def map_new_schema_to_columns(self, columns, schema):
        """
        updates the column names in socrata metadata 
        to the values in schema def
        """
        schema_col_dict = {col['api_name']: col for col in schema['columns']}

        for column in columns:
            col_name = column['field_name']
            if col_name in schema_col_dict:
                new_col = schema_col_dict[col_name]
                column['format'] = new_col.get(
                    'format', column['format'])
                #see column formatting options https://socratapublishing.docs.apiary.io/introduction/examples/column-formatting-update
                column['description'] = new_col.get(
                    'description', column['description'])
                column['display_name'] = new_col.get(
                    'name', column['display_name'])
                # TODO: this destroys existing data. investigate why and fix.
                column['field_name'] = new_col.get(
                    'rename_api', column['field_name'])
                # TODO: delete column[key] where values are None (this overwrites whatever is currently on platform)
        return columns

    def make_metadata(self, schema, default_values):
        """
        make json metadata object (for socrata updating) from schema def
        """
        properties = [
            'title',
            'description',
            'steward_name',
            'steward_email',
            'publishing_frequency',
            'data_change_frequency',
            'geographic_unit',
            'publishing_lag_in_days',
            'publishing_lag',
            'publishing_method',
            'automation_candidate',
            'category',
            'department',
            'tags',
        ]
        vals = {prop: schema.get(
            prop, default_values[prop]) for prop in properties}
        metadata = {
            "tags": vals['tags'],
            "privateMetadata": {
                "custom_fields": {
                    "Internal Management": {
                        "Publishing lag in days": vals['publishing_lag_in_days'],
                        "Publishing lag": vals['publishing_lag'],
                        "Publishing Method": vals['publishing_method'],
                        "Automation Candidate": vals['automation_candidate'],
                        "Data Steward name": vals['steward_name'],
                        "Data Steward email": vals['steward_email']
                    }
                }
            },
            "name": vals['title'],
            "metadata": {
                "custom_fields": {
                    "Publishing Details": {
                        "Publishing frequency": vals['publishing_frequency'],
                        "Data change frequency": vals['data_change_frequency'],
                    },
                    "Detailed Descriptive": {
                        "Geographic unit": vals['geographic_unit']
                    },
                    "Department Metrics": {
                        "Publishing Department": vals['department']
                    }
                },
            },
            "licenseId": "PDDL",
            "license": {
                "termsLink": "http://opendatacommons.org/licenses/pddl/1.0/",
                "name": "Open Data Commons Public Domain Dedication and License"
            },
            "description": vals['description'],
            "category": vals['category'],
        }
        return metadata

    def make_full_schema(self, meta_response):
        """
        make schema def (for yaml output) from socrata metadata response
        """
        xwalk = [
            ('name', 'name'),
            ('datatype', 'dataTypeName'),
            ('description', 'description'),
            ('api_name', 'fieldName'),
        ]
        columns = [{k: col[v] for (k, v) in xwalk}
                   for col in meta_response['columns']]

        # metadata = self.parse_metadata_response(meta_response)
        metadata =  {'title': meta_response['name']}
        metadata['columns'] = columns
        metadata['id'] = meta_response['id']

        return metadata

    def parse_metadata_response(self, meta_response):
        metadata = {
            'title':                    meta_response['name'],
            'description':              meta_response['description'],
            'steward_name':             meta_response['privateMetadata']['custom_fields']['Internal Management']['Data Steward name'],
            'steward_email':            meta_response['privateMetadata']['custom_fields']['Internal Management']['Data Steward email'],
            'publishing_frequency':     meta_response['metadata']['custom_fields']['Publishing Details']['Publishing frequency'],
            'data_change_frequency':    meta_response['metadata']['custom_fields']['Publishing Details']['Data change frequency'],
            'geographic_unit':          meta_response['metadata']['custom_fields']['Detailed Descriptive']['Geographic unit'],
            'publishing_lag':           meta_response['privateMetadata']['custom_fields']['Internal Management']['Publishing lag'],
            'publishing_lag_in_days':   meta_response['privateMetadata']['custom_fields']['Internal Management']['Publishing lag in days'],
            'publishing_method':        meta_response['privateMetadata']['custom_fields']['Internal Management']['Publishing Method'],
            'automation_candidate':     meta_response['privateMetadata']['custom_fields']['Internal Management']['Automation Candidate'],
            'category':                 meta_response['category'],
            'department':               meta_response['metadata']['custom_fields']['Department Metrics']['Publishing Department'],
            'tags':                     meta_response['tags'],
        }
        return metadata

    def format_columns_create_step1(self, schema):
        colprops = [
            'name',
            'datatype',
            ]
        switcher = {
            'integer': 'number',
            'money': 'number',
            'real': 'number',
        }
        res = copy.deepcopy(schema)
        for r in res['columns']:
            if (r['datatype'] in switcher):
                r['datatype'] = switcher[r['datatype']]
            for p in list(r):
                if p not in colprops:
                    r.pop(p, None)
        return res

    def format_columns_create_step2(self, schema, fourfour):
        colprops = [
            'name',
            'description',
            'datatype',
            'format',
            'rename_api',
            'api_name'
            ]
        switcher = {
            'integer': 'number',
            'money': 'number',
            'real': 'number',
        }    
        res = copy.deepcopy(schema)
        for r in res['columns']:
            if (r['datatype'] in switcher):
                r['datatype'] = switcher[r['datatype']]
            r['api_name'] = r['api_name_guess']
            for p in list(r):
                if p not in colprops:
                    r.pop(p, None)
        res['id'] = fourfour
        return res

    # main logic ops    
    def create(self, schema):
        """
        create a new dataset using a schema file
        
        Parameters
        ----------
        schema_name : str 
            convenience name for the schema definition

        Returns
        -------
            log entries
            writes schema to file
        """
        schema_name = schema.get('schema_name')
        if ('id' in schema):
            raise Exception(f'Provided {schema_name} already has "id" property; likely it has been created') 
        
        create_url = f'{self.domain_url}/api/publishing/v1/revision'
        create_json = {
            'metadata': {
                'name': schema['title']
            },
            'action': {
                'type': 'update',
                'permission': 'private'
            }
        }
        source_body_data = {
            'source_type': {
                'type': 'upload',
                'filename': 'initial_data.csv'
            },
            'parse_options': {
                'parse_source': 'true'
            }
        }
         
        dataset_file = os.path.join(
            self.csv_create_filepath, f'{schema_name}.csv')

        with open(dataset_file, "rb") as f:
            file_bin_data = f.read()
        f.closed

        revision_response = self.create_dataset_revision(
            create_url, create_json)
        source_response = self.create_source(
            revision_response, source_body_data)
        upload_response = self.upload_file(source_response, file_bin_data)
        
        # This number will always be 0 for the first publication.
        # Then it will increment up by one each time a new revision is created.
        revision_number = revision_response.json()['resource']['revision_seq']

        apply_body = {
            'resource': {
                'id': revision_number
            }
        }
        
        apply_revision_response = self.apply_revision(
            revision_response, apply_body, stage = 'create')
        
        # save out the fourfour for the new dataset
        fourfour = revision_response.json()['resource']['fourfour']
        schema.update({'id': fourfour})
        
        # update the schema column's 'api_name' with created_col's 'field_name'
        # always schema 0 because this is the first iteration
        created_cols = upload_response.json()['resource']['schemas'][0]['input_columns']
        new_cols = {}
        for col in created_cols:
            new_cols.setdefault(col['display_name'], {}).update(col)
        # match schema.columns['name'] with created_cols['display_name']
        for col in schema['columns']:
            col['api_name'] = new_cols[col['name']]['field_name']

        # TODO REFACTOR output fourfourid to extant
        self.fs_save_out(schema, schema_name)

        return fourfour
    
    def update(self, schema, from_scratch=False):
        """
        Update a dataset's metadata using a yaml schema definition.
        Can be used for new datasets or to update existing datasets.
        Can update both dataset metadata and column metadata.
        
        Parameters
        ----------
        schema : dict 
            schema definition

        from_scratch : bool
            set to True if this is being called as part of a create
            dataset from scratch recipe, which will fill in the required DataSF 
            metadata using defaults defined in __init__()

        Returns
        -------
            log entries
            writes schema to file
        """
        schema_name = schema.get('schema_name')
        fourfour = schema.get('id')
        logger.debug(f'updating metadata for {schema_name} {fourfour}')
        if from_scratch:
            metameta = self.default_meta
        else:
            metameta = self.get_metadata(fourfour)
            metameta = self.parse_metadata_response(metameta)

        s1_url = f'{self.domain_url}/api/publishing/v1/revision/{fourfour}'
        s1_body = {
            'metadata': self.make_metadata(schema, metameta),
            'action': {
                'type': 'replace',
                'permission': self.permission
                # TODO: "default" permission seems to be public, figure out
                # way to read "current" permission and set accordingly
            }
        }
        s2_body = {
            'source_type': {
                'type': 'view',
                'fourfour': fourfour
            }
        }
                
        rev_response = self.create_dataset_revision(s1_url, s1_body)
        create_source_response = self.create_source(
            rev_response, s2_body)
        transform_schema_response = self.transform_schema(
            create_source_response, schema)

        if transform_schema_response.status_code == 400:
            logging.debug(transform_schema_response.json())

        apply_body = {
            'output_schema_id': transform_schema_response.json()[
            'resource']['id']
        }
        apply_response = self.apply_revision(
            rev_response,
            apply_body,
            stage='create'
        )

        schema = self.update_schema_columns(
            schema, transform_schema_response.json())
        self.fs_save_out(schema, schema_name)

        print(apply_response.json())

    def read(self, schema_name, fourfour):
        """
        Get the yaml schema definition for existing dataset        
        
        Parameters
        ----------
        schema_name : str 
            convenience name for the schema definition

        fourfour : str
            socrata identifier for the dataset

        Returns
        -------
            log entries
            writes schema to yaml file
        """
        metadata = self.get_metadata(fourfour)
        schema = self.make_full_schema(metadata)
        self.fs_save_out(schema, schema_name)

    def delete(self, schema_name, fourfour, autobackup=True):
        """
        delete a dataset's data (replace with blank csv source)
        automatically saves a backup copy to ./backup/{fourfour}.csv

        Parameters
        ----------
        schema_name : str 
            convenience name for the schema definition

        autobackup: boolean
            whether to download a csv copy of the dataset from socrata
            default True

        Returns
        -------
            log entries
        """
        # schema_name = schema.get('schema_name')
        # fourfour = schema.get('id')
        logger.debug(f'deleting all data for {schema_name} {fourfour}')
        
        if autobackup:
            self.backup_dataset(fourfour)

        s1_url = f'{self.domain_url}/api/publishing/v1/revision/{fourfour}'
        s1_body = {
            'action': {
                'type': 'replace',
                'permission': self.permission
                # TODO: "default" permission seems to be public, figure out
                # way to read "current" permission and set accordingly
            }
        }
        s2_body = {
            'source_type': {
                'type': 'upload',
                'filename': 'blank_data.csv'
            }
        }

        dataset_file = os.path.join(
            self.csv_create_filepath, f'{schema_name}_blank.csv')

        with open(dataset_file, "rb") as f:
            file_bin_data = f.read()
        f.closed
        
        rev_response = self.create_dataset_revision(s1_url, s1_body)
        source_response = self.create_source(
            rev_response, s2_body)

        upload_response = self.upload_file(source_response, file_bin_data)

        revision_number = rev_response.json()['resource']['revision_seq']

        apply_body = {
            'resource': {
                'id': revision_number
            }
        }

        apply_response = self.apply_revision(
            rev_response,
            apply_body
        )

        logger.info(apply_response.json())
    
    def replace(self, schema, filename):
        """
        replace a dataset's data 
        
        Parameters
        ----------
        schema_name : str 
            convenience name for the schema definition
        
        schema : dict
            from yaml definition
        
        filename : str
            filename for csv file to upload (must be in "input" folder)

        Returns
        -------
            log entries
        """
        schema_name = schema.get('schema_name')
        fourfour = schema.get('id')
        logger.debug(f'replacing data for {schema_name} {fourfour}')

        s1_url = f'{self.domain_url}/api/publishing/v1/revision/{fourfour}'
        s1_body = {
            'action': {
                'type': 'replace',
                'permission': self.permission
                # TODO: "default" permission seems to be public, figure out
                # way to read "current" permission and set accordingly
            }
        }
        s2_body = {
            'source_type': {
                'type': 'upload',
                'filename': 'new_data.csv'
            }
        }

        dataset_file = os.path.join(
            self.root_path, filename)

        with open(dataset_file, "rb") as f:
            file_bin_data = f.read()
        f.closed
        
        rev_response = self.create_dataset_revision(s1_url, s1_body)
        source_response = self.create_source(
            rev_response, s2_body)

        upload_response = self.upload_file(source_response, file_bin_data)

        revision_number = rev_response.json()['resource']['revision_seq']

        apply_body = {
            'resource': {
                'id': revision_number
            }
        }

        # attempting to send the apply command too soon after uploading
        # a file can error as the columns need to finish processing
        time.sleep(5)
        apply_response = self.apply_revision(
            rev_response,
            apply_body
        )


        print(apply_response.json())
