import os
import re
import sys
import yaml
from csv import DictReader, DictWriter

"""
Creates a yaml definition from a csv file and a yaml file
named "schema_name.yaml" and "schema_name.csv"
both have to be saved in folder "schemas/input"
yaml file should at minimum have properties describing dataset:
    title, description, tags
csv file should have headers:
    name,description,datatype,api_rename


"""

def read_csv(file_path):
    read_obj = open(file_path, 'r')
    dict_reader = DictReader(read_obj)
    schema = list(dict_reader)
    return schema

def read_schema_yaml(file_path):
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)
    return data

def standardize_name(name):
    name = re.sub("[^A-Za-z0-9 ]", "", name)
    return name

def guess_api_name(name):
    api_name = re.sub(" ","_", name)
    return api_name.lower()

def default_formatting(datatype):
    switcher = {
        'date': {'view': 'date'},
        'integer': {'precision': 0, 'noCommas': True, 'align': 'right'},
        'real': {'precision': 2, 'align': 'right'},
        'money': {
            'precisionStyle': 'currency',
            'currencyStyle': 'USD',
            'precision': 2,
            'noCommas': False,
            'align': 'right'
            }
    }
    if (datatype in switcher):
        return switcher[datatype]
    return None
    
def write_csv(file_path, dict_data, csv_columns):
    with open(file_path, 'w') as csvfile:
        writer = DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        writer.writerows(dict_data)

def make_fake_data(datatype):
    switcher = {
        'string': 'A String',
        'boolean': 'true',
        'date': '2020-01-31',
        'integer': '5',
        'real': '2.54',
        'money': '1.45'
    }
    return switcher.get(datatype, None)

def plump_col(d):
    if ('datatype' in d):
        d['format'] = default_formatting(d['datatype'])
    d['name'] = standardize_name(d['name'])
    d['api_name_guess'] = guess_api_name(d['name'])
    return d

def write_yaml(file_path, data):
    with open(file_path, 'w') as file:
        yaml.safe_dump(data, file,
                       default_flow_style=False, sort_keys=False)

def make_schema(schema_name):
    """
    creates a fake csv file using the columns defined in the schema definition
    """
    csv_file = os.path.join(
        sys.path[0], 'schemas', 'input', f'{schema_name}_columns.csv')
    schema_metadata_file = os.path.join(
        sys.path[0], 'schemas', 'input', f'{schema_name}_metadata.yaml')
    root_schema_file = os.path.join(
        sys.path[0], 'schemas', 'root', f'{schema_name}.yaml')
    csv_create_file = os.path.join(
        sys.path[0], 'schemas', 'create', f'{schema_name}.csv')  
    csv_blank_file = os.path.join(
        sys.path[0], 'schemas', 'create', f'{schema_name}_blank.csv')    
    columns = read_csv(csv_file)
    
    schema = read_schema_yaml(schema_metadata_file)
        
    # schema['columns'] = [(d) for d in columns]
    schema['schema_name'] = schema_name
    schema['columns'] = [plump_col(dict(d)) for d in columns]
    template = {}

    for col in schema['columns']:
        template[col['name']] = make_fake_data(col['datatype'])

    write_csv(csv_create_file, [template], list(template.keys()))
    write_csv(csv_blank_file, [], list(template.keys()))
    write_yaml(root_schema_file, schema)
    
if __name__ == '__main__':
    if len(sys.argv) >= 2:
        filename = sys.argv[1]
        make_schema(filename)
        print(f'created {filename} root schema definition')
    else:
        print('expected an argument: schema_name')

